# Rewards API Documentation

Welcome to the Rewards API documentation. This API allows you to create new rewards for businesses. Rewards can be of different types, including percentage discounts, fixed amount discounts, and free items.

## Create Rewards Endpoint

```
POST /crm_api/create/business/rewards
```

This endpoint creates a new reward based on the provided input data.

### Request Body

- **parent_business_id** (string, required): ID of the parent business.
- **title** (string): Title of the offer.
- **description** (string): Description of the offer.
- **reward_type** (string): Type of the reward.
- **validity** (array): Array containing valid from and valid till dates.
- **validHours** (array): Array containing valid hours from and to.
- **max_allowed_per_customer** (number): Maximum allowed per customer.
- **min_cart_qty** (number): Minimum item quantity in the cart.
- **wallet_amount** (number): Amount redeemable with wallet points.
- **milestone_level** (string): Minimum milestone level required.
- **not_valid_at** (array): Array of business IDs where the reward is not valid.
- **only_valid_at** (array): Array of business IDs where the reward is valid.
- **valid_for** (array): Array of valid types (1 - DineIn, 2 - Takeaway, 3 - Delivery).
- **rewards_type** (number, required): Type of the reward (1, 2, or 3).

#### Reward Types

1. **Percentage Discount** (`rewards_type: 1`)
   - **discount_min** (number): Minimum order amount for discount.
   - **discount_percentage** (number): Percentage of discount.
   - **discount_max** (number): Maximum discount amount.

2. **Fixed Amount Discount** (`rewards_type: 2`)
   - **discount_min** (number): Minimum order amount for discount.
   - **discount_amount** (number): Fixed discount amount.
   - **discount_max** (number): Maximum discount amount.

3. **Free Item** (`rewards_type: 3`)
   - **free_item_id** (string): ID of the free item.
   - **discount_min** (number): Minimum order amount for freebie.
   - **with_other_item** (array): Array of item IDs for freebie.

### Response

- **Success (201)**: The reward was successfully created.
  ```json
  {
    "status": 1,
    "msg": "Success",
    "coupon_id": "COUPON123"
  }
  ```

- **Failure (403)**: Bad request or reward creation failed.
  ```json
  {
    "status": 0,
    "msg": "Bad Request"
  }
  ```

- **Failure (500)**: Internal server error.
  ```json
  {
    "status": 0,
    "msg": "Internal Server Error"
  }
  ```

### Example Usage

```
{
  "parent_business_id": "BUSINESS123",
  "title": "Summer Sale",
  "description": "Get amazing discounts on summer items.",
  "reward_type": "Discount",
  "validity": ["2023-08-01", "2023-08-15"],
  "validHours": [9, 18],
  "max_allowed_per_customer": 1,
  "min_cart_qty": 3,
  "wallet_amount": 20,
  "milestone_level": "Silver",
  "not_valid_at": [],
  "only_valid_at": [],
  "valid_for": [1, 2],
  "rewards_type": 1,
  "discount_min": 50,
  "discount_percentage": 20,
  "discount_max": 100
}
```